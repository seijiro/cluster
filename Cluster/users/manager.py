# -*- coding: utf-8 -*-
from django.db import models


class FollowManager(models.Manager):
    """
    FollowのManager
    """
    def get_following(self, user, count=0):
        """
        フォローユーザを取得
        """
        if count > 0:
            return self.filter(from_user=user, is_active=True)[:count]
        else:
            return self.filter(from_user=user, is_active=True)

    def get_follower(self, user, count=0):
        """
        フォロワーを取得
        """
        if count > 0:
            return self.filter(target_user=user, is_active=True)[:count]
        else:
            return self.filter(target_user=user, is_active=True)
