# -*- coding: utf-8 -*-
from django.contrib import admin
from .models import Profile, Picture, Follow
from django.contrib.auth.models import User


class ProfileInline(admin.StackedInline):
    model = Profile
    can_delete = False


class UserAdmin(admin.ModelAdmin):
    inlines = (ProfileInline, )


admin.site.unregister(User)
admin.site.register(User, UserAdmin)


class ProfileAdmin(admin.ModelAdmin):
    date_hierarchy = 'modified'
    list_display = (
        'name',
        'user',
        'thumbnail',
        'homepage',
        'self_introduction',
        'created',
        'modified',
    )
admin.site.register(Profile, ProfileAdmin)


class PictureAdmin(admin.ModelAdmin):
    date_hierarchy = 'modified'
    list_display = (
        'picture',
        'created',
        'modified',
    )


admin.site.register(Picture, PictureAdmin)


class FollowAdmin(admin.ModelAdmin):
    date_hierarchy = 'modified'
    list_display = (
        'from_user',
        'target_user',
        'created',
        'modified',
    )


admin.site.register(Follow, FollowAdmin)
