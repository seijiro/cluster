from django.contrib import admin
from .models import Game, Category, Community, Thread


class GameAdmin(admin.ModelAdmin):
    date_hierarchy = 'created'

admin.site.register(Game, GameAdmin)


class CategoryAdmin(admin.ModelAdmin):
    date_hierarchy = 'created'
