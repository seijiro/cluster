
from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.models import User


class Game(models.Model):
    name = models.CharField(_('name'), max_length=20)
    is_active = models.BooleanField(default=True)
    created = models.DateTimeField(_('created'), auto_now=True)

    def __str__(self):
        return self.name


class Category(models.Model):
    name = models.CharField(_('name'), max_length=12)
    created = models.DateTimeField(_('created'), auto_now=True)
    updated = models.DateTimeField(_('updated'), auto_now_add=True)

    def __str__(self):
        return self.name


class Community(models.Model):
    game = models.ForeignKey(Game)
    name = models.CharField(_('name'), max_length=20)
    description = models.TextField(_('description'))

    def __str__(self):
        return self.name


class Thread(models.Model):
    community = models.ForeignKey(Community)
    name = models.CharField(_('name'), max_length=20)
    user = models.ForeignKey(User)
    value = models.TextField(_('text vallue'))
    created = models.DateTimeField(_('created'), auto_now=True)
    updated = models.DateTimeField(_('updated'), auto_now=True)

    def __str__(self):
        return self.name
