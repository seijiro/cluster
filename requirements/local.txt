-r base.txt
coverage==3.6
django-discover-runner==0.4
django-debug-toolbar==0.9.4
django-pdb==0.3.2
django-extensions==1.2.1
Sphinx==1.2b1
Werkzeug==0.9.4

